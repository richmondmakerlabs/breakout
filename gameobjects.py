#!/usr/bin/env python2

from kivy.properties import (ListProperty, NumericProperty,
                             ObjectProperty, StringProperty)

    class Game(FloatLayout):
        blocks = ListProperty([])
        player = ObjectProperty()
        ball = ObjectProperty()

    class Player(Widget):
        position = NumericProperty(0.5)
        direction = StringProperty('none')

    class Ball(Widget):
        pos_hint_x = NumericProperty(0.5)
        pos_hint_y = NumericProperty(0.3)
        proper_size = NumericProperty(0.)
        velocity = ListProperty([0.1, 0.5])

    class Block(Widget):
        colour = ListProperty([1, 0, 0])

    
