#!/usr/bin/env python2

from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle

class Player(Widget):
    def __init__(self, **kwargs):
        super(Player, self).__init__(**kwargs)
        with self.canvas:
            Color(1,0,0,1)
            Rectangle(pos=self.pos, size=self.size)
            
