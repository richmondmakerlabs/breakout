#!/usr/bin/env python2

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.modalview import ModalView
from kivy.properties import (ListProperty, NumericProperty,
                             ObjectProperty, StringProperty)
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle


__version__ = '0.1' 

class BreakoutApp(App):
    
    class Game(FloatLayout):
        blocks = ListProperty([])
        player = ObjectProperty()
        ball = ObjectProperty()
        def setup_blocks(self):
            for y_jump in range(5):
                for x_jump in range(10):
                    block = Block(pos_hint={
                        'x': 0.05 + 0.09*x_jump,
                        'y': 0.05 + 0.09*y_jump})
                    self.blocks.append(block)
                    self.add_widget(block)
                    

    class Player(Widget):
        position = NumericProperty(0.5)
        direction = StringProperty('none')
        def __init__(self, **kwargs):
            super(Player, self).__init__(**kwargs)
            with self.canvas:
                Color(1,0,0,1)
                Rectangle(pos=self.pos, size=self.size)




    class Ball(Widget):
        pos_hint_x = NumericProperty(0.5)
        pos_hint_y = NumericProperty(0.3)
        proper_size = NumericProperty(0.)
        velocity = ListProperty([0.1, 0.5])

    class Block(Widget):
        colour = ListProperty([1, 0, 0])
        def __init__(self, **kwargs):
            self.colour = random.choice([
                (0.78, 0.28, 0), (0.28, 0.63, 0.28), (0.25, 0.28, 0.78)])
        
    def build(self):
        g = Game()
        g.setup_blocks()
        return g

    pass

BreakoutApp().run()




            
